# example deployment

`docker-compose` based example deployment, very similar to my actual running stack.

inspired largely by https://gitlab.com/Deamos/open-streaming-platform-docker/-/merge_requests/4

`misc-services.yaml` includes Matrix.org Synapse+Element chat service as well as Keycloak OpenID to tie it all together.

## issues you might have attempting to run it out of the box

uses a private container of the OSP nightly/ branch.

the ejabberd container is built from `Dockerfile.ejabberd` and `Dockerfile.migration` includes the ejabberd SQL schema to finish instantiating it's MySQL DB.


### misc notes
docker-compose v2.x because v3 requires a wildly different Docker deployment than I feel like in order to support resource control directives like:

```
    cpus: 2.0
    mem_limit: 256m
    memswap_limit: 512m
    mem_reservation: 256m
```

these are of course, optional. I am simply trying to keep my bills down for now. you may want to remove or adjust them for your own deployment.

`ejabberd` `mysql.new.sql` schema resulted in errors, but the "older" schema worked. both are committed for reference, but only one is included into the `migration` container.

I had issues with the `tmpfs` directive, which might be a docker-compose v2 vs v3 issue. I didn't debug at all, just switched to classic volume so the data started writing to something at all.
